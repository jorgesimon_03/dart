import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:busqueda/busqueda_usr.dart';

void main() {
  runApp(const ListaUsers());
  //runApp(const MyApp());
}

class MyApp extends StatelessWidget{
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Contenedor(),
        bottomNavigationBar: BarraNavegacion(),
      ),
    );
  }
}


class Contenedor extends StatelessWidget{
  const Contenedor({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: const [
          BarraBusqueda(),
          OpcionesBusqueda()
        ],
    );
  }
}

class BarraBusqueda extends StatelessWidget{
  const BarraBusqueda({super.key});

  @override
  Widget build(BuildContext context) {
    return const CupertinoTextField(placeholder: "Busca lo que imagines",);
  }
}

class OpcionesBusqueda extends StatelessWidget{
  const OpcionesBusqueda({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
        //margin: const EdgeInsets.only(top: 20),
        child: Row(
          children: const [
            Expanded(
                child: Icon(Icons.person)
            ),
            Expanded(
                child: Icon(Icons.map)
            )
          ],
        )
    );
  }
}

class BarraNavegacion extends StatelessWidget{
  const BarraNavegacion({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: const [
        Expanded(child: Icon(Icons.home)),
        Expanded(child: Icon(Icons.add)),
        Expanded(child: Icon(Icons.person)),
      ],
    );
  }
}
