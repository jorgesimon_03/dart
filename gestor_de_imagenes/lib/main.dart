import 'package:flutter/material.dart';
import 'package:gestor_de_imagenes/login.dart';

void main() {
  runApp(
      const MaterialApp(
          debugShowCheckedModeBanner: false,
          home: MiEstructura()
      )
  );
}

class MyApp extends StatelessWidget{
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Crear una columna con tres filas"),
      ),
      body: Container(
        alignment: Alignment.center,
        child: Column(
          children: const [
            Apartados(
              miCadena: "Hacer foto.",
              miIcon: Icon(
                  Icons.add_a_photo
              ),
            ),
            Apartados(
              miCadena: "Compartir.",
              miIcon: Icon(
                  Icons.share
              ),
            ),
            Apartados(
              miCadena: "Eliminar foto.",
              miIcon: Icon(
                  Icons.delete
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class Apartados extends StatelessWidget{
  final String miCadena;
  final Icon miIcon;

  const Apartados({
    required this.miCadena,
    required this.miIcon
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(miCadena),
        miIcon
      ],
    );
  }
}
